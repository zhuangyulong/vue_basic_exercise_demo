import Vue from 'vue';
import Router from 'vue-router';
import App from '../components/HelloWorld';
import AppItem from '../components/items/test';
import Cart from '../components/cart/cart';
import Home from '../components/home/home';

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      component: App
    },
    {
      path: '/app',
      name: 'app',
      component: App,
      children: [{
        path: 'item',
        name: 'appItem',
        component: AppItem
      }]
    },
    {
      path: '/cart',
      component: Cart
    },
    {
      path: '/home',
      component: Home
    },
    {
      path: '/item',
      component: AppItem
    }
  ]
})
