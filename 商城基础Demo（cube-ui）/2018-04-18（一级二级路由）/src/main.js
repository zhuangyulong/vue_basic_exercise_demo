// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Cube from 'cube-ui';
import App from './App';
import router from './router';
import Header from './components/common/header'; //头部组件
import FooterList from './components/common/footerList'; //底部导航列表组件
import View from './components/common/view';

import './common/stylus/style_1.styl'; //引入字体图标

Vue.use(Cube)

Vue.component('my-header', Header);
Vue.component('my-footer', FooterList);
Vue.component('my-view', View);


Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
